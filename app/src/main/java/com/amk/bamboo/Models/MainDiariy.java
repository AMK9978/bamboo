package com.amk.bamboo.Models;

import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.Models.Hashtags.Hashtag;

import java.util.ArrayList;

public class MainDiariy {
    private Hashtag hashtag;
    private ArrayList<Diary> diaries = new ArrayList<>();

    public Hashtag getHashtag() {
        return hashtag;
    }

    public void setHashtag(Hashtag hashtag) {
        this.hashtag = hashtag;
    }

    public ArrayList<Diary> getDiaries() {
        return diaries;
    }

    public void setDiaries(ArrayList<Diary> diaries) {
        this.diaries = diaries;
    }

    @Override
    public String toString() {
        return "MainDiariy{" +
                "hashtag=" + hashtag +
                ", diaries=" + diaries +
                '}';
    }
}
