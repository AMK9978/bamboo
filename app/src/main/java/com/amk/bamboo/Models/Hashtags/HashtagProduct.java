package com.amk.bamboo.Models.Hashtags;

public class HashtagProduct {
    private int id, hashtag_id, product_id,
    priority = 0;
    private int isSpecial = 1;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHashtag_id() {
        return hashtag_id;
    }

    public void setHashtag_id(int hashtag_id) {
        this.hashtag_id = hashtag_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int isSpecial() {
        return isSpecial;
    }

    public void setSpecial(int special) {
        isSpecial = special;
    }
}
