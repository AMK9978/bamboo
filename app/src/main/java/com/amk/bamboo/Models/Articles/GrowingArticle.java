package com.amk.bamboo.Models.Articles;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GrowingArticle implements Serializable {
    private int id = 0;
    private String slug = "", content = "", description = "", imagePath = "";
    @SerializedName("title")
    private String title = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GrowingArticle{" +
                "id=" + id +
                ", slug='" + slug + '\'' +
                ", content='" + content + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", imagePath='" + imagePath + '\'' +
                '}';
    }
}
