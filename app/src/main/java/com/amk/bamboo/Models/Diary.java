package com.amk.bamboo.Models;

import com.amk.bamboo.Models.Hashtags.DiaryHashtag;
import com.amk.bamboo.Models.Hashtags.Hashtag;

import java.util.ArrayList;

public class Diary {
    private int isPrivate = 0, isDraft = 0, forOther = 0;
    private String note;
    private int id, baby_id;
    private ArrayList<DiaryImage> diaryImages = new ArrayList<>();
    private ArrayList<Hashtag> hashtags = new ArrayList<>();
    private String imagePath = "";

    public ArrayList<DiaryImage> getDiaryImages() {
        return diaryImages;
    }

    public void setDiaryImages(ArrayList<DiaryImage> diaryImages) {
        this.diaryImages = diaryImages;
    }

    public ArrayList<Hashtag> getHashtags() {
        return hashtags;
    }

    public void setHashtags(ArrayList<Hashtag> hashtags) {
        this.hashtags = hashtags;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int isPrivate() {
        return isPrivate;
    }

    public void setPrivate(int aPrivate) {
        isPrivate = aPrivate;
    }

    public int isDraft() {
        return isDraft;
    }

    public void setDraft(int draft) {
        isDraft = draft;
    }

    public int isForOther() {
        return forOther;
    }

    public void setForOther(int forOther) {
        this.forOther = forOther;
    }

    public int getIsPrivate() {
        return isPrivate;
    }

    public void setIsPrivate(int isPrivate) {
        this.isPrivate = isPrivate;
    }

    public int getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(int isDraft) {
        this.isDraft = isDraft;
    }

    public int getForOther() {
        return forOther;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBaby_id() {
        return baby_id;
    }

    public void setBaby_id(int baby_id) {
        this.baby_id = baby_id;
    }

    @Override
    public String toString() {
        return "Diary{" +
                "note='" + note + '\'' +
                '}';
    }
}
