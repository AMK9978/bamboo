package com.amk.bamboo.Models.Theme;

import java.sql.Date;

public class ThemeSuggestion {
    private int id, product_id;
    private String related_product, shorten_url;
    private Date start_at, end_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getRelated_product() {
        return related_product;
    }

    public void setRelated_product(String related_product) {
        this.related_product = related_product;
    }

    public String getShorten_url() {
        return shorten_url;
    }

    public void setShorten_url(String shorten_url) {
        this.shorten_url = shorten_url;
    }

    public Date getStart_at() {
        return start_at;
    }

    public void setStart_at(Date start_at) {
        this.start_at = start_at;
    }

    public Date getEnd_at() {
        return end_at;
    }

    public void setEnd_at(Date end_at) {
        this.end_at = end_at;
    }
}
