package com.amk.bamboo.Models.Theme;

import java.sql.Date;

public class ThemeHistory {
    private int id, user_id, product_id,
            theme_suggestion_id;
    private Date suggested_at;
    private String message;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getTheme_suggestion_id() {
        return theme_suggestion_id;
    }

    public void setTheme_suggestion_id(int theme_suggestion_id) {
        this.theme_suggestion_id = theme_suggestion_id;
    }

    public Date getSuggested_at() {
        return suggested_at;
    }

    public void setSuggested_at(Date suggested_at) {
        this.suggested_at = suggested_at;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
