package com.amk.bamboo.Models.Hashtags;

public class DiaryImageHashtag {
    private int id, diary_image_id, hashtag_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDiary_image_id() {
        return diary_image_id;
    }

    public void setDiary_image_id(int diary_image_id) {
        this.diary_image_id = diary_image_id;
    }

    public int getHashtag_id() {
        return hashtag_id;
    }

    public void setHashtag_id(int hashtag_id) {
        this.hashtag_id = hashtag_id;
    }
}
