package com.amk.bamboo.Models.Hashtags;

import java.sql.Date;

public class Hashtag {
    private int isActive = 1, justForProducts = 0;
    private String hashtag = "", slug = "";
    private String type = "";
    private int product_id;
    private Date start_at, ent_at;
    private int isMain = 0;
    private String imagePath = "";


    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int isMain() {
        return isMain;
    }

    public void setMain(int main) {
        isMain = main;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int isActive() {
        return isActive;
    }

    public void setActive(int active) {
        isActive = active;
    }

    public int isJustForProducts() {
        return justForProducts;
    }

    public void setJustForProducts(int justForProducts) {
        this.justForProducts = justForProducts;
    }

    public String getHashtag() {
        return hashtag;
    }

    public void setHashtag(String hashtag) {
        this.hashtag = hashtag;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public Date getStart_at() {
        return start_at;
    }

    public void setStart_at(Date start_at) {
        this.start_at = start_at;
    }

    public Date getEnt_at() {
        return ent_at;
    }

    public void setEnt_at(Date ent_at) {
        this.ent_at = ent_at;
    }
}
