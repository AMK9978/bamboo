package com.amk.bamboo.Models.Shop;

public class Coupon {
    private int id;
    private int isActive = 1, logged = 0,
            free_shipping = 0, discounted_products = 0;
    private String name, code, type, start_at, end_at,
            user_total, user_customer;
    private double amount = 0, max_amount, min_amount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int isActive() {
        return isActive;
    }

    public void setActive(int active) {
        isActive = active;
    }

    public int isLogged() {
        return logged;
    }

    public void setLogged(int logged) {
        this.logged = logged;
    }

    public int isFree_shipping() {
        return free_shipping;
    }

    public void setFree_shipping(int free_shipping) {
        this.free_shipping = free_shipping;
    }

    public int isDiscounted_products() {
        return discounted_products;
    }

    public void setDiscounted_products(int discounted_products) {
        this.discounted_products = discounted_products;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStart_at() {
        return start_at;
    }

    public void setStart_at(String start_at) {
        this.start_at = start_at;
    }

    public String getEnd_at() {
        return end_at;
    }

    public void setEnd_at(String end_at) {
        this.end_at = end_at;
    }

    public String getUser_total() {
        return user_total;
    }

    public void setUser_total(String user_total) {
        this.user_total = user_total;
    }

    public String getUser_customer() {
        return user_customer;
    }

    public void setUser_customer(String user_customer) {
        this.user_customer = user_customer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getMax_amount() {
        return max_amount;
    }

    public void setMax_amount(double max_amount) {
        this.max_amount = max_amount;
    }

    public double getMin_amount() {
        return min_amount;
    }

    public void setMin_amount(double min_amount) {
        this.min_amount = min_amount;
    }
}
