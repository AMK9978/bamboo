package com.amk.bamboo.Models.Products;

import android.net.Uri;

import java.io.Serializable;

public class Product {
    private int id, cod_id, weight, stock,
            image_id, download, discount_id;
    private int isVisible = 1,
            hasTax = 0, hasShipping = 0, hasPage = 0,
            isCountable = 1;
    private String title, code, description, content;
    private double price;
    private Discount discount = null;
    private String imagePath = "", bannerPath = "", thumbPath = "";

    public String getBannerPath() {
        return bannerPath;
    }

    public void setBannerPath(String bannerPath) {
        this.bannerPath = bannerPath;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }

    public int getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(int isVisible) {
        this.isVisible = isVisible;
    }

    public int getHasTax() {
        return hasTax;
    }

    public void setHasTax(int hasTax) {
        this.hasTax = hasTax;
    }

    public int getHasShipping() {
        return hasShipping;
    }

    public void setHasShipping(int hasShipping) {
        this.hasShipping = hasShipping;
    }

    public int getHasPage() {
        return hasPage;
    }

    public void setHasPage(int hasPage) {
        this.hasPage = hasPage;
    }

    public int getIsCountable() {
        return isCountable;
    }

    public void setIsCountable(int isCountable) {
        this.isCountable = isCountable;
    }

    public int getDiscount_id() {
        return discount_id;
    }

    public void setDiscount_id(int discount_id) {
        this.discount_id = discount_id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Discount getDiscount() {
        return discount;
    }

    public void setDiscount(Discount discount) {
        this.discount = discount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCod_id() {
        return cod_id;
    }

    public void setCod_id(int cod_id) {
        this.cod_id = cod_id;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public int getDownload() {
        return download;
    }

    public void setDownload(int download) {
        this.download = download;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
