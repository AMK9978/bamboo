package com.amk.bamboo.Models.Hashtags;

public class HashtagSuggestions {
    private int id, hashtag_id;
    private String title, suggestion,
            user_sentence;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHashtag_id() {
        return hashtag_id;
    }

    public void setHashtag_id(int hashtag_id) {
        this.hashtag_id = hashtag_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getUser_sentence() {
        return user_sentence;
    }

    public void setUser_sentence(String user_sentence) {
        this.user_sentence = user_sentence;
    }
}
