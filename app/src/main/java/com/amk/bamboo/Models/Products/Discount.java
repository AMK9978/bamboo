package com.amk.bamboo.Models.Products;

import java.io.Serializable;

public class Discount {
    private int id, product_id, min_quantity = 0;
    private int isSpecial = 0;
    private double price = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getMin_quantity() {
        return min_quantity;
    }

    public void setMin_quantity(int min_quantity) {
        this.min_quantity = min_quantity;
    }

    public int isSpecial() {
        return isSpecial;
    }

    public void setSpecial(int special) {
        isSpecial = special;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
