package com.amk.bamboo.Models.Shop;

public class Payment {
    private int id, user_id;
    private String bank_order_id = "0",
            type, referenceCode = "0", track_code = "0",
            bank, payed_at;
    private double amount = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getBank_order_id() {
        return bank_order_id;
    }

    public void setBank_order_id(String bank_order_id) {
        this.bank_order_id = bank_order_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getReferenceCode() {
        return referenceCode;
    }

    public void setReferenceCode(String referenceCode) {
        this.referenceCode = referenceCode;
    }

    public String getTrack_code() {
        return track_code;
    }

    public void setTrack_code(String track_code) {
        this.track_code = track_code;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getPayed_at() {
        return payed_at;
    }

    public void setPayed_at(String payed_at) {
        this.payed_at = payed_at;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
