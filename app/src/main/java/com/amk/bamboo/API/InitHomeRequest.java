package com.amk.bamboo.API;

import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.Models.MainDiariy;
import com.amk.bamboo.Models.User;

import java.util.ArrayList;

public class InitHomeRequest {
    private ArrayList<Diary> diaries = new ArrayList<>();
    private ArrayList<MainDiariy> mainDiaries = new ArrayList<>();
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Diary> getDiaries() {
        return diaries;
    }

    public void setDiaries(ArrayList<Diary> diaries) {
        this.diaries = diaries;
    }

    public ArrayList<MainDiariy> getMainDiaries() {
        return mainDiaries;
    }

    public void setMainDiaries(ArrayList<MainDiariy> mainDiaries) {
        this.mainDiaries = mainDiaries;
    }

    @Override
    public String toString() {
        return "InitHomeRequest{" +
                "diaries=" + diaries +
                ", mainDiaries=" + mainDiaries +
                '}';
    }
}
