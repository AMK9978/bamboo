package com.amk.bamboo.API;

import com.amk.bamboo.Models.Articles.GrowingArticle;
import com.amk.bamboo.Models.Articles.GrowingArticleImages;

import java.util.ArrayList;

public class ArticlesRequest {
    private ArrayList<GrowingArticle> articles = new ArrayList<>();
    private ArrayList<GrowingArticleImages> articleImages = new ArrayList<>();

    public ArrayList<GrowingArticle> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<GrowingArticle> articles) {
        this.articles = articles;
    }

    public ArrayList<GrowingArticleImages> getArticleImages() {
        return articleImages;
    }

    public void setArticleImages(ArrayList<GrowingArticleImages> articleImages) {
        this.articleImages = articleImages;
    }

    /*
     "articles": [
        {
            "id": 1,
            "title": "شش ماهگی",
            "description": "رشد گوش ها",
            "content": "در این دوره گوش ها خیلی رشد میکنند!",
            "imagePath": null
        },
        {
            "id": 2,
            "title": "Nothing",
            "description": "HICH",
            "content": "Salam",
            "imagePath": "http://127.0.0.1:8000/path"
        }
    ],
    "article_images": [
        {
            "growing_article_id": 1,
            "description": null,
            "imagePath": "http://127.0.0.1:8000/path"
        }
    ],
    "article_paragraphs": [
        {
            "growing_article_id": 1,
            "title": "پارچه بخر",
            "description": "پارچه خیلی بخر",
            "content": "خیلی خیلی بخر",
            "imagePath": "http://127.0.0.1:8000/path"
        }
    ]
    * */
}
