package com.amk.bamboo.API;

import com.amk.bamboo.Models.Articles.GrowingArticle;
import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.Models.Hashtags.DiaryHashtag;
import com.amk.bamboo.Models.Hashtags.Hashtag;
import com.amk.bamboo.Models.Products.ProductCategory;
import com.amk.bamboo.Models.User;
import com.amk.bamboo.Temp;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface APIInterface {

    //AUTHENTICATION//////////////////////

    @POST("signup")
    @FormUrlEncoded
    Call<User> signUp(@Field("name") String name, @Field("mobile") String mobile,
                      @Field("password") String password, @Field("birthday") String birthday,
                      @Field("gender") String gender);

    @POST("login")
    @FormUrlEncoded
    Call<User> login(@Field("username") String username,
                     @Field("password") String password);


    @POST("requestSMS")
    @FormUrlEncoded
    Call<Temp> sendPhoneNumber(@Field("mobile") String mobile);


    @POST("checkSMS")
    @FormUrlEncoded
    Call<User> sendVerificationCode(@Field("mobile") String mobile, @Field("lookup") String lookup);

    @POST("requestSMS")
    @FormUrlEncoded
    Call<DiaryHashtag> getDiaryHashtags(@Field("diary") Diary diary);

    //////////////////////////////////////

    @GET("products")
    Call<ArrayList<ProductCategory>> getProducts();

    @GET("initHome")
    Call<InitHomeRequest> getInitHome();

    @GET("hashtags")
    Call<ArrayList<Hashtag>> getHashtags();

    @Multipart
    @POST("postCreateDiary")
    Call<Temp> postCreateDiary(@Field("diary") Diary diary,
                               @Part MultipartBody.Part file);

    @GET("articles")
    Call<ArticlesRequest> getArticles();

}
