package com.amk.bamboo.API;

import com.amk.bamboo.Util;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static Retrofit retrofit = null;
    private static final String BASE_URL = "http://10.0.2.2:8000/api/v1/";
    private static OkHttpClient client = new OkHttpClient.Builder().addInterceptor(chain -> {
        String token = Util.sharedPreferences.getString("jwt", "nothing");
        Request newRequest = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer " + token)
                .build();
        return chain.proceed(newRequest);
    }).build();

    public static Retrofit getRetrofit() {
        if (retrofit == null) {
            return new Retrofit.Builder().client(client).baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }
}
