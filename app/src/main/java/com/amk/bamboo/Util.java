package com.amk.bamboo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.amk.bamboo.Auth.LoginActivity;
import com.amk.bamboo.Auth.SignUp;
import com.google.firebase.FirebaseApp;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Util extends AppCompatActivity {

    public static Typeface iranSans;
    public static Typeface iranSans_bold;
    public static Typeface iranSans_normal;
    public static Typeface iranSans_medium;
    public static Typeface iranSans_fanum;
    public static Typeface iranSans_light;

    public static SharedPreferences sharedPreferences;
    public static Typeface iranSans_ultra_light;
    public static Typeface typeface1;
    public static Typeface typeface2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = getSharedPreferences("pref", MODE_PRIVATE);
        typeface1 = getFont(getAssets(), "fonts/AGhasem.ttf");
        iranSans = getFont(getAssets(), "fonts/iransans.ttf");
        iranSans_fanum = getFont(getAssets(), "fonts/iran_sans_fanum.ttf");
        typeface2 = getFont(getAssets(), "fonts/iran_sans_fanum.ttf");
        iranSans_normal = getFont(getAssets(), "fonts/iran_sans_normal.ttf");
        iranSans_medium = getFont(getAssets(), "fonts/iran_sans_medium.ttf");
        iranSans_light = getFont(getAssets(), "fonts/iran_sans_light.ttf");
        iranSans_ultra_light = getFont(getAssets(), "fonts/iran_sans_ultra_light.ttf");
        iranSans_bold = getFont(getAssets(), "fonts/iran_sans_bold.ttf");
        FirebaseApp.initializeApp(this);
        Intent intent = new Intent(getApplicationContext(), Splash.class);
        startActivity(intent);
        finish();
    }

    public Typeface getFont(AssetManager assetManager, String path) {
        return Typeface.createFromAsset(assetManager, path);
    }

    public static Dialog getNewDialog(Context context, int viewID, boolean cancelable) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setCancelable(cancelable);
        dialog.setContentView(viewID);
        return dialog;
    }

    /**
     * we use api sweetDialog ,please see Gradle
     *
     * @param type sweet dialog have type to show
     * @Return SweetAlertDialog for activitys
     */
    public static SweetAlertDialog getSweetAlertDialog(Context context, int type) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, type);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setOnShowListener(dialogInterface -> {
            SweetAlertDialog alertDialog = (SweetAlertDialog) dialogInterface;
            TextView text = alertDialog.findViewById(R.id.title_text);
            text.setTypeface(typeface2);
            text = alertDialog.findViewById(R.id.content_text);
            text.setTypeface(typeface2);
            Button clButton = alertDialog.findViewById(R.id.cancel_button);
            clButton.setTypeface(typeface2);
            clButton = alertDialog.findViewById(R.id.neutral_button);
            clButton.setTypeface(typeface2);
            clButton = alertDialog.findViewById(R.id.confirm_button);
            clButton.setTypeface(typeface2);
        });
        return dialog;
    }


    public static boolean isNetworkConnected(Context context) {
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();

        if (activeNetworkInfo != null) {
            // connected to the internet
            // connected to the mobile provider's data plan
            if (activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else return activeNetworkInfo.getType() == ConnectivityManager.TYPE_MOBILE;
        }
        return false;
    }


}
