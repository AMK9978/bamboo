package com.amk.bamboo.DashboardFragments;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.API.ArticlesRequest;
import com.amk.bamboo.Adapters.BannerAdapter;
import com.amk.bamboo.Adapters.ProductCategoryAdapter;
import com.amk.bamboo.EventFragment;
import com.amk.bamboo.Models.Products.ProductCategory;
import com.amk.bamboo.R;
import com.amk.bamboo.Util;
import com.rd.PageIndicatorView;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsFragment extends Fragment {

    private RecyclerView categoryRecycler;
    private ProductCategoryAdapter categoryAdapter;
    private ArticlesRequest request;
    private ViewPager viewPager;
    private SweetAlertDialog dialog;
    private PageIndicatorView indicatorView;
    private int item = 0;
    private BannerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.products_fragment_layout, container, false);
        viewPager = view.findViewById(R.id.banner_view_pager);
        indicatorView = view.findViewById(R.id.pageIndicatorView);
        categoryRecycler = view.findViewById(R.id.category_recycler);

        adapter = new BannerAdapter(getFragmentManager());
        adapter.addFragment(new EventFragment(Uri.parse("https://chandmahame.com/storage/products/baner-barf.jpg")));
        adapter.addFragment(new EventFragment(Uri.parse("https://chandmahame.com/storage/products/baner-barf.jpg")));
        viewPager.setAdapter(adapter);
        viewPager.setScrollBarFadeDuration(1000);
        indicatorView.setCount(adapter.getCount());
        indicatorView.setViewPager(viewPager);

        APIInterface apiInterface = APIClient.getRetrofit().create(APIInterface.class);
        dialog = Util.getSweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        dialog.setTitle(getResources().getString(R.string.wait));
        dialog.show();
        apiInterface.getProducts().enqueue(new Callback<ArrayList<ProductCategory>>() {
            @Override
            public void onResponse(Call<ArrayList<ProductCategory>> call, Response<ArrayList<ProductCategory>> response) {
                if (response.isSuccessful()) {
                    setRecycler(response.body());
                } else {
                    Log.i("TAG", "Error in onResponse of Products " + response.code());
                    Log.i("TAG", String.valueOf(response.body()));
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ArrayList<ProductCategory>> call, Throwable t) {
                dialog.dismiss();
                dialog = Util.getSweetAlertDialog(
                        getContext(), SweetAlertDialog.ERROR_TYPE);
                dialog.setTitle(getResources().getString(R.string.error));
                dialog.setContentText(getResources().getString(R.string.connection_error));
                dialog.show();
                dialog.setConfirmText(getResources().getString(R.string.ok));
                dialog.setConfirmClickListener(view -> dialog.dismiss());
                Log.i("TAG", t.getMessage());
            }
        });
        return view;
    }

    private void setRecycler(ArrayList<ProductCategory> productCategories) {
        categoryAdapter = new ProductCategoryAdapter(productCategories, getContext());
        categoryRecycler.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        categoryRecycler.setAdapter(categoryAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
//        new Thread(() -> {
//            while (true) {
//                try {
//                    Thread.sleep(3000);
//                    Log.i("TAG", String.valueOf(item));
//                    getActivity().runOnUiThread(() -> {
//                        viewPager.setCurrentItem(item++);
//                        if (item == adapter.getCount()) {
//                            item = 0;
//                        }
//                    });
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }).start();
    }

    @Override
    public void onStop() {
        super.onStop();
        dialog.dismiss();
    }

    public static Fragment newInstance() {
        return new ProductsFragment();
    }

}
