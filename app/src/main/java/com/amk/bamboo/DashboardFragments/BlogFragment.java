package com.amk.bamboo.DashboardFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.API.ArticlesRequest;
import com.amk.bamboo.Adapters.ArticleAdapter;
import com.amk.bamboo.R;
import com.amk.bamboo.Util;

import org.jetbrains.annotations.NotNull;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BlogFragment extends Fragment {

    private RecyclerView recyclerView;
    private ArticleAdapter adapter;
    private ArticlesRequest request;
    private SweetAlertDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.blog_fragment_layout, container, false);
        recyclerView = view.findViewById(R.id.articles_recycler);
        APIInterface apiInterface = APIClient.getRetrofit().create(APIInterface.class);
        dialog = Util.getSweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        dialog.setTitle(getResources().getString(R.string.wait));
        dialog.show();
        apiInterface.getArticles().enqueue(new Callback<ArticlesRequest>() {
            @Override
            public void onResponse(@NotNull Call<ArticlesRequest> call, @NotNull Response<ArticlesRequest> response) {
                if (response.isSuccessful()) {
                    request = response.body();
                    if (request != null) {
                        adapter = new ArticleAdapter(request.getArticles(), getContext());
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
                        recyclerView.setAdapter(adapter);
                        dialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<ArticlesRequest> call, @NotNull Throwable t) {
                dialog.dismiss();
                dialog = Util.getSweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE);
                dialog.setTitle(getResources().getString(R.string.error));
                dialog.setContentText(getResources().getString(R.string.connection_error));
                dialog.show();
                dialog.setConfirmText(getResources().getString(R.string.ok));
                dialog.setConfirmClickListener(view -> dialog.dismiss());
            }
        });
        return view;
    }
    public static Fragment newInstance() {
        return new BlogFragment();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (dialog.isShowing())
            dialog.dismiss();
    }
}
