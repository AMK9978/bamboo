package com.amk.bamboo.DashboardFragments;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.system.ErrnoException;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.Adapters.ImagesAdapter;
import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.R;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.yalantis.ucrop.UCrop;
import com.yalantis.ucrop.model.AspectRatio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static android.app.Activity.RESULT_OK;

public class PrintFragment extends Fragment {
    private final int PIC_CROP = 1;
    private RecyclerView images_recycler;
    private ImageView addImage;
    private ImageView uploadedImage;
    private TextView warning;
    private ArrayList<Diary> diaries = new ArrayList<>();
    private Animation add_image_anim;
    private int galleryReqCode = 100;
    private int permissionCode = 101;
    private ImagesAdapter imagesAdapter;
    private Button printImages;

    public PrintFragment() {
    }

    public static Fragment newInstance() {
        return new PrintFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.print_layout_fragment, container, false);
        images_recycler = view.findViewById(R.id.your_images);
        add_image_anim = AnimationUtils.loadAnimation(getContext(), R.anim.add_image_anim);
        addImage = view.findViewById(R.id.add_image);
        uploadedImage = view.findViewById(R.id.uploadedImage);
        warning = view.findViewById(R.id.warning_txt);
        addImage.startAnimation(add_image_anim);
        imagesAdapter = new ImagesAdapter(diaries, getContext());
        images_recycler.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        images_recycler.setAdapter(imagesAdapter);
        printImages = view.findViewById(R.id.printImages);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        addImage.setOnClickListener(v -> pickImageFromGallery());
        APIInterface apiInterface = APIClient.getRetrofit().create(APIInterface.class);

        printImages.setOnClickListener(v -> {

        });

        return view;
    }

    private void addImage(Uri resultUri) {
        Diary diary = new Diary();
        diary.setImagePath(resultUri.toString());
        diaries.add(diary);
        imagesAdapter.notifyItemInserted(diaries.size() - 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);
                Log.i("TAGgg", resultUri.toString());
                uploadedImage.setVisibility(View.VISIBLE);
                Picasso.get().load(resultUri).placeholder(R.drawable.ic_person_outline_blue_48dp).into(uploadedImage);
                warning.setVisibility(View.GONE);
                printImages.setVisibility(View.VISIBLE);
                addImage(resultUri);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                Log.i("TAGgg", cropError.getMessage());
            }
            if (requestCode == galleryReqCode) {
                if (data.getData() != null)
                    performCrop(data.getData());
            }
            if (requestCode == PIC_CROP) {
                // get the returned data
                Bundle extras = data.getExtras();
                // get the cropped bitmap
                Bitmap selectedBitmap = extras.getParcelable("data");

                uploadedImage.setImageBitmap(selectedBitmap);
            }
        }
    }

    private void performCrop(Uri picUri) {
        Log.i("TAG", String.valueOf(picUri));
        File file = new File(Environment.getExternalStorageDirectory(), "ChandMahame");
        String ss = new Random().nextInt(9999999) + ".png";
        File myPathFile = new File(file.getAbsolutePath() + "/" + ss);
        UCrop.Options options = new UCrop.Options();
        options.setToolbarTitle("برش تصویر");
        options.setToolbarColor(getResources().getColor(R.color.colorPrimary));
        options.setLogoColor(getResources().getColor(R.color.colorPrimary));
        options.setStatusBarColor(getResources().getColor(R.color.white));
        options.setActiveWidgetColor(getResources().getColor(R.color.colorPrimary));
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setToolbarWidgetColor(getResources().getColor(R.color.white));
        options.setActiveWidgetColor(getResources().getColor(R.color.colorPrimary));
        options.setAspectRatioOptions(1,
                new AspectRatio("نسبت دو به یک", 2, 1),
                new AspectRatio("نسبت برابر", 1, 1));
        UCrop.of(picUri, Uri.fromFile(myPathFile)).withOptions(options)
                .start(getContext(), this);
//        try {
//            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), picUri);
//            uploadedImage.setImageBitmap(bitmap);
////            cropImageView.
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        try {
//            Intent cropIntent = new Intent("com.android.camera.action.CROP");
//            // indicate image type and Uri
//            cropIntent.setDataAndType(picUri, "image/*");
//            // set crop properties here
//            cropIntent.putExtra("crop", true);
//            // indicate aspect of desired crop
//            cropIntent.putExtra("aspectX", 1);
//            cropIntent.putExtra("aspectY", 1);
//            // indicate output X and Y
//            cropIntent.putExtra("outputX", 128);
//            cropIntent.putExtra("outputY", 128);
//            // retrieve data on return
//            cropIntent.putExtra("return-data", true);
//            // start the activity - we handle returning in onActivityResult
//            startActivityForResult(cropIntent, PIC_CROP);
//        }
//        // respond to users whose devices do not support the crop action
//        catch (ActivityNotFoundException anfe) {
//        }
    }

    private void pickImageFromGallery() {
//        startActivityForResult(getPickImageChooserIntent(), 200);
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, galleryReqCode);
    }

    /**
     * Create a chooser intent to select the source to get image from.<br/>
     * The source can be camera's (ACTION_IMAGE_CAPTURE) or gallery's (ACTION_GET_CONTENT).<br/>
     * All possible sources are added to the intent chooser.
     */
    private Intent getPickImageChooserIntent() {

        // Determine Uri of camera image to save.
        Uri outputFileUri = getCaptureImageOutputUri();

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = getContext().getPackageManager();

        // collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            }
            allIntents.add(intent);
        }

        // collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

        // the main intent is the last in the list (fucking android) so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.amk.Bamboo.PrintFragment")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

        // Create a chooser from the main intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "منبع را مشخص کنید");

        // Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }


    /**
     * Get URI to image received from capture by camera.
     */
    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = getContext().getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    private void saveIntoStorage(Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        File file = new File(Environment.getExternalStorageDirectory(), "ChandMahame");
        if (!file.exists())
            file.mkdir();
        String path = "profileImage.jpg";
        File myPathFile = new File(file.getAbsolutePath() + "/" + path);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(myPathFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the URI of the selected image from {@link #getPickImageChooserIntent()}.<br/>
     * Will return the correct URI for camera and gallery image.
     *
     * @param data the returned data of the activity result
     */
    private Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }


    /**
     * Test if we can open the given Android URI to test if permission required error is thrown.<br>
     */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private boolean isUriRequiresPermissions(Uri uri) {
        try {
            ContentResolver resolver = getContext().getContentResolver();
            InputStream stream = resolver.openInputStream(uri);
            stream.close();
            return false;
        } catch (FileNotFoundException e) {
            if (e.getCause() instanceof ErrnoException) {
                return true;
            }
        } catch (Exception ignored) {
        }
        return false;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(getActivity());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionCode == requestCode) {
            if (grantResults.length != 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                pickImageFromGallery();
            }
        }
    }

}
