package com.amk.bamboo.DashboardFragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.API.InitHomeRequest;
import com.amk.bamboo.Adapters.MainDiariesAdapter;
import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.Models.Hashtags.Hashtag;
import com.amk.bamboo.R;
import com.amk.bamboo.Util;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private RecyclerView diaries_recycler;
    private MainDiariesAdapter adapter;
    private ImageView printImage, registerDiary, blog, fProduct, sProduct, addDiary;
    private SweetAlertDialog dialog;
    private APIInterface apiInterface;
    private ArrayList<Hashtag> mainDiaryHashtags = new ArrayList<>();
    private ArrayList<Diary> diaries = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.home_fragment_layout, container, false);
        addDiary = root.findViewById(R.id.addDiary);
        diaries_recycler = root.findViewById(R.id.diaries);
        printImage = root.findViewById(R.id.print_image);
        registerDiary = root.findViewById(R.id.register_diary);
        blog = root.findViewById(R.id.blog);
        fProduct = root.findViewById(R.id.first_product);
        sProduct = root.findViewById(R.id.second_product);
        apiInterface = APIClient.getRetrofit().create(APIInterface.class);

        apiInterface.getInitHome().enqueue(new Callback<InitHomeRequest>() {
            @Override
            public void onResponse(@NotNull Call<InitHomeRequest> call, @NotNull Response<InitHomeRequest> response) {
                if (response.isSuccessful() && response.body() != null) {
                    setRecycler(response.body());
                } else {
                    Log.i("TAG", "Error in onResponse of Products " + response.code());
                }
            }

            @Override
            public void onFailure(@NotNull Call<InitHomeRequest> call, @NotNull Throwable t) {
                Log.i("TAG", t.getMessage());
            }
        });
        addDiary.setOnClickListener(v -> loadFragment("gotoRegister"));
        printImage.setOnClickListener(v -> loadFragment("gotoPrint"));
        blog.setOnClickListener(v -> loadFragment("gotoBlog"));
        registerDiary.setOnClickListener(v -> loadFragment("gotoRegister"));
        sProduct.setOnClickListener(v -> loadFragment("gotoProducts"));
        fProduct.setOnClickListener(v -> loadFragment("gotoProducts"));

        dialog = Util.getSweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        dialog.setTitle(getResources().getString(R.string.wait));
//        dialog.show();
        prepareHome();
        diaries_recycler.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
//        diaries_recycler.setAdapter(adapter);
        return root;
    }

    private void setRecycler(InitHomeRequest initHomeRequest) {
        /*
        *   categoryAdapter = new ProductCategoryAdapter(productCategories, getContext());
        categoryRecycler.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        categoryRecycler.setAdapter(categoryAdapter);
        * */
        Log.i("TAG", "JJJJJJJJJJJJJJJJ");
        Log.i("TAG", initHomeRequest.getMainDiaries().toString());
        adapter = new MainDiariesAdapter(initHomeRequest.getMainDiaries(), getContext());
        diaries_recycler.setAdapter(adapter);
        diaries_recycler.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
    }

    private void loadFragment(String key) {
        Fragment fragment;
        switch (key) {
            case "gotoRegister":
                fragment = new RegisterDiaryFragment();
                break;
            case "gotoPrint":
                fragment = new PrintFragment();
                break;
            case "gotoBlog":
                fragment = new BlogFragment();
                break;
            default:
                fragment = new ProductsFragment();
                break;
        }
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void prepareHome() {

        /*
        dialog.dismiss();
                dialog = Util.getSweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE);
                dialog.setTitle(getResources().getString(R.string.error));
                dialog.setContentText(getResources().getString(R.string.connection_error));
                dialog.show();
                dialog.setConfirmText(getResources().getString(R.string.ok));
                dialog.setConfirmClickListener(view -> dialog.dismiss());
        * */
    }

}
