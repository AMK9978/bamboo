package com.amk.bamboo;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.amk.bamboo.Models.Articles.GrowingArticle;
import com.squareup.picasso.Picasso;

public class ArticleActivity extends AppCompatActivity {

    private GrowingArticle growingArticle;
    private ImageView articleImage;
    private TextView title, content, description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        articleImage = findViewById(R.id.article_image);
        title = findViewById(R.id.title);
        description = findViewById(R.id.description);
        content = findViewById(R.id.content);
        growingArticle = (GrowingArticle) getIntent().getExtras().get("article");
        Uri imageUri = Uri.parse(growingArticle.getImagePath());
        Log.i("TAG", imageUri.toString());
        Picasso.get().load(imageUri).placeholder(R.drawable.baner_albumeha).into(articleImage);
        title.setText(growingArticle.getTitle());
        description.setText(growingArticle.getDescription());
        content.setText(growingArticle.getContent());
    }
}
