package com.amk.bamboo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

public class ProductActivity extends AppCompatActivity {

    ImageView product_image, shareBtn, backBtn;
    TextView productTitle, content, details, price;
    Button addToBasket;
    int image_id;
    int weight;
    String contentText;
    int stock, id;
    String code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        product_image = findViewById(R.id.product_image);
        shareBtn = findViewById(R.id.share_btn);
        backBtn = findViewById(R.id.backBtn);
        productTitle = findViewById(R.id.product_title);
        content = findViewById(R.id.content);
        details = findViewById(R.id.details);
        price = findViewById(R.id.price);
        addToBasket = findViewById(R.id.addToBasket);
        backBtn.setOnClickListener(v -> onBackPressed());
        addToBasket.setOnClickListener(v -> {
            //TODO: Call laravel api

        });
        setWidgets();
        if (getIntent().getExtras().get("link") != null) {
            Log.i("TAG", getIntent().getExtras().get("link").toString());
            Uri link = Uri.parse((String) getIntent().getExtras().get("link"));
            Picasso.get().load((link)).into(product_image);
        }
        shareBtn.setOnClickListener(v -> share());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setWidgets() {
        String priceText = (String) getIntent().getExtras().get("price");
        String detailText = (String) getIntent().getExtras().get("content");
        id = (Integer) getIntent().getExtras().get("id");
        String titleText = (String) getIntent().getExtras().get("title");
        image_id = (Integer) getIntent().getExtras().get("image_id");
        weight = (Integer) getIntent().getExtras().get("weight");
        contentText = (String) getIntent().getExtras().get("description");
        stock = (Integer) getIntent().getExtras().get("stock");
        code = (String) getIntent().getExtras().get("code");
        details.setText(detailText);
        price.setText(priceText);
        productTitle.setText(titleText);
        content.setText(contentText);
    }


    private void share() {
        ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setChooserTitle("اشتراک گذاری چندماهمه")
                .setText("https://morris_fa.ir/morris.apk")
                .startChooser();
    }
}
