package com.amk.bamboo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.Models.Products.Product;
import com.amk.bamboo.ProductActivity;
import com.amk.bamboo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.Item> {


    private ArrayList<Product> products;
    private Context context;

    public ProductAdapter(ArrayList<Product> products, Context context) {
        this.products = products;
        this.context = context;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_layout, parent, false);
        return new ProductAdapter.Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        Product product = products.get(position);
        holder.title.setText(product.getTitle());
        holder.price.setText(String.valueOf(product.getPrice()));
        if (holder.discount != null) {
            holder.discount.setVisibility(View.VISIBLE);
        }
        Picasso.get().load(product.getImagePath()).into(holder.productImage);
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, ProductActivity.class);
            intent.putExtra("title", product.getTitle());
            intent.putExtra("content", product.getContent());
            intent.putExtra("description", product.getDescription());
            intent.putExtra("id", product.getId());
            intent.putExtra("image_id", product.getImage_id());
            intent.putExtra("discount_id", product.getDiscount_id());
            intent.putExtra("link", product.getImagePath());
            intent.putExtra("weight", product.getWeight());
            intent.putExtra("stock", product.getStock());
            intent.putExtra("code", product.getCode());
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    class Item extends RecyclerView.ViewHolder {
        TextView title, price;
        ImageView productImage, discount;

        Item(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.product_title);
            price = itemView.findViewById(R.id.price);
            productImage = itemView.findViewById(R.id.product_image);
            discount = itemView.findViewById(R.id.discount);
            discount.setVisibility(View.GONE);
        }
    }
}
