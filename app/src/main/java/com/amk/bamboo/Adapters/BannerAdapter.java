package com.amk.bamboo.Adapters;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mList = new ArrayList<>();

    public BannerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int i) {
        return mList.get(i);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    public void addFragment(Fragment fragment) {
        mList.add(fragment);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Nothing";
    }
}

