package com.amk.bamboo.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.Item> {

    private ArrayList<Diary> diaries;
    private Context context;

    public ImagesAdapter(ArrayList<Diary> diaries, Context context) {
        this.diaries = diaries;
        this.context = context;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.print_image_layout, parent, false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        Diary diary = diaries.get(position);
        holder.title.setText(diary.getNote());
        Picasso.get().load(diary.getImagePath()).placeholder(R.drawable.ic_person_outline_blue_48dp)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return diaries.size();
    }


    class Item extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;

        Item(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.print_image);
            title = itemView.findViewById(R.id.diary_title);
        }
    }
}
