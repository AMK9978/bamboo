package com.amk.bamboo.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.Models.Products.Product;
import com.amk.bamboo.Models.Products.ProductCategory;
import com.amk.bamboo.R;

import java.util.ArrayList;

public class ProductCategoryAdapter extends RecyclerView.Adapter<ProductCategoryAdapter.Item> {


    private ArrayList<ProductCategory> productCategories;
    private Context context;

    public ProductCategoryAdapter(ArrayList<ProductCategory> productCategories, Context context) {
        this.productCategories = productCategories;
        this.context = context;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.product_categroy_layout, parent, false);
        return new ProductCategoryAdapter.Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        ProductCategory productCategory = productCategories.get(position);
        holder.title.setText(productCategory.getTitle());
        ProductAdapter productAdapter = new ProductAdapter(productCategory.getProducts(), context);
        holder.recyclerView.setAdapter(productAdapter);
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.HORIZONTAL, false));
    }

    @Override
    public int getItemCount() {
        return productCategories.size();
    }


    class Item extends RecyclerView.ViewHolder {
        TextView title;
        RecyclerView recyclerView;

        Item(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.category_title);
            recyclerView = itemView.findViewById(R.id.products_recycler);
        }
    }
}
