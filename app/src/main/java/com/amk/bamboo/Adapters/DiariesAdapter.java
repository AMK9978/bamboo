package com.amk.bamboo.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.Models.Attachment;
import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.Models.DiaryImage;
import com.amk.bamboo.Models.Hashtags.DiaryHashtag;
import com.amk.bamboo.R;

import java.io.File;
import java.util.ArrayList;

public class DiariesAdapter extends RecyclerView.Adapter<DiariesAdapter.Item>  {

    private ArrayList<Diary> diaries;
    private ArrayList<DiaryHashtag> diaryHashtags;
    private ArrayList<DiaryImage> diaryImages;
    private Context context;

    public DiariesAdapter(ArrayList<Diary> diaries,
                          Context context) {
        this.diaries = diaries;
        this.context = context;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.diary_bar_layout, parent, false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        DiaryHashtag diaryHashtag = diaryHashtags.get(position);
        Diary diary = diaries.get(diaryHashtag.getDiary_id());
        DiaryImage diaryImage = null;
        for (int i = 0; i < diaryImages.size(); i++) {
            if (diaryImages.get(i).getDiary_id() == diary.getId()){
                 diaryImage = diaryImages.get(i);
                 break;
            }
        }
        if (diaryImage != null){
            Log.i("TAG", diaryImage.toString());
//            try {
//                Log.i("TAG", String.valueOf(this.carpets.get(position).getPath()));
//                Uri uri = Uri.fromFile(new File(String.valueOf()));
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
//                holder.carpetImage.setImageBitmap(Bitmap.createScaledBitmap(bitmap
//                        , dpToPx(165), dpToPx(150), false));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }else{

        }
    }

    @Override
    public int getItemCount() {
        return diaries.size();
    }

    class Item extends RecyclerView.ViewHolder {
        TextView hashtag_title;
        ImageView diaryImageHashtag;
        
        Item(@NonNull View itemView) {
            super(itemView);
            diaryImageHashtag = itemView.findViewById(R.id.diary_bar_image);
            hashtag_title = itemView.findViewById(R.id.diary_bar_title);
        }
    }

    private int dpToPx(int dp) {
        float density = context.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
}
