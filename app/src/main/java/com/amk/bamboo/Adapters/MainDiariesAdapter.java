package com.amk.bamboo.Adapters;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.Models.Diary;
import com.amk.bamboo.Models.DiaryImage;
import com.amk.bamboo.Models.Hashtags.DiaryHashtag;
import com.amk.bamboo.Models.Hashtags.Hashtag;
import com.amk.bamboo.Models.MainDiariy;
import com.amk.bamboo.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

public class MainDiariesAdapter extends RecyclerView.Adapter<MainDiariesAdapter.Item> {

    private ArrayList<MainDiariy> mainDiaries;
    private Context context;

    public MainDiariesAdapter(ArrayList<MainDiariy> diaries,
                              Context context) {
        this.mainDiaries = diaries;
        this.context = context;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.diary_bar_layout, parent, false);
        return new Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        MainDiariy mainDiariy = mainDiaries.get(position);
        Hashtag hashtag = mainDiariy.getHashtag();
        ArrayList<Diary> diaries = mainDiariy.getDiaries();
        holder.hashtag_title.setText(hashtag.getHashtag());
        int len = diaries.size();
        Random random = new Random();
        Log.i("TAG", len + " ");
        if (len > 0) {
            int random_int = random.nextInt(len);
            Picasso.get().load(diaries.get(random_int).getImagePath()).placeholder(R.drawable.ic_person_silverlight_24dp)
                    .into(holder.diaryImageHashtag);
        }else{
            Picasso.get().load(Uri.parse("https://images.unsplash.com/photo-1523895665936-7bfe172b757d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"))
                    .into(holder.diaryImageHashtag);
        }
    }

    @Override
    public int getItemCount() {
        return mainDiaries.size();
    }

    class Item extends RecyclerView.ViewHolder {
        TextView hashtag_title;
        ImageView diaryImageHashtag;

        Item(@NonNull View itemView) {
            super(itemView);
            diaryImageHashtag = itemView.findViewById(R.id.diary_bar_image);
            hashtag_title = itemView.findViewById(R.id.diary_bar_title);
        }
    }

    private int dpToPx(int dp) {
        float density = context.getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
}
