package com.amk.bamboo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amk.bamboo.ArticleActivity;
import com.amk.bamboo.Models.Articles.GrowingArticle;
import com.amk.bamboo.R;

import java.util.ArrayList;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.Item> {

    private ArrayList<GrowingArticle> growingArticles;
    private Context context;

    public ArticleAdapter(ArrayList<GrowingArticle> growingArticles, Context context) {
        this.growingArticles = growingArticles;
        this.context = context;
    }

    @NonNull
    @Override
    public Item onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.articles_rec_layout, parent, false);
        return new ArticleAdapter.Item(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Item holder, int position) {
        GrowingArticle article = growingArticles.get(position);
        holder.article_description.setText(article.getDescription());
//        holder.articleCounter.setText(article.getId());
//        holder.article_text_counter.setText("هفته " + article.getId());
        Log.i("TAG", article.toString());
        holder.article_title.setText(article.getTitle());
        holder.shareBtn.setOnClickListener(view -> {

        });
        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, ArticleActivity.class);
            intent.putExtra("article", article);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return growingArticles.size();
    }

    class Item extends RecyclerView.ViewHolder {
        TextView article_title, article_text_counter, article_description;
        ImageView articleImage, shareBtn;
        Button more_info, articleCounter;

        Item(@NonNull View itemView) {
            super(itemView);
            articleImage = itemView.findViewById(R.id.article_image);
            article_text_counter = itemView.findViewById(R.id.article_text_counter);
            article_title = itemView.findViewById(R.id.article_title);
            more_info = itemView.findViewById(R.id.more_info);
            shareBtn = itemView.findViewById(R.id.share_btn);
            article_description = itemView.findViewById(R.id.article_description);
            articleCounter = itemView.findViewById(R.id.article_counter);
        }
    }
}
