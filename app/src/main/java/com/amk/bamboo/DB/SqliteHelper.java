package com.amk.bamboo.DB;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class SqliteHelper extends SQLiteOpenHelper {
    static final String DB_NAME = "chandmahame_db";
    static final String BABIES_TABLE_NAME = "babies";
    static final String USER_ID = "user_id";
    static final String IMAGE_ID = "image_id";
    static final String NAME = "name";
    static final String BIRTHDAY = "birthday";
    static final String GENDER = "gender";
    static final String IS_DEFAULT = "isDefault";

    static final String DIARIES_TABLE_NAME = "diaries";
    static final String IS_PRIVATE = "isPrivate";
    static final String IS_DRAFT = "isDraft";
    static final String FOR_OTHER = "forOther";
    static final String NOTE = "note";
    static final String BABY_ID = "baby_id";

    static final String IMAGE_DIARIES_TABLE_NAME = "diaryImages";
    static final String DIARY_ID = "diary_id";
    static final String REVIEWED = "reviewed";

    static final String HASHTAG_DIARIES_TABLE_NAME = "hashtag_diaries";
    static final String HASHTAG_ID = "hashtag_id";

    //private int id, diary_id, hashtag_id;
    public SqliteHelper(@Nullable Context context) {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table if not exists " + BABIES_TABLE_NAME + "( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + USER_ID + " INTEGER, " + IMAGE_ID + " INTEGER," + NAME + " TEXT," + BIRTHDAY + " TEXT," + GENDER + " TEXT, "+
                IS_DEFAULT + " BOOLEAN)");

        db.execSQL("create table if not exists " + DIARIES_TABLE_NAME + "( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + IS_PRIVATE + " boolean, " + IS_DRAFT + " boolean," + FOR_OTHER + " boolean," +
                NOTE + " TEXT," + BABY_ID + " INTEGER)");

        db.execSQL("create table if not exists " + IMAGE_DIARIES_TABLE_NAME + "( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DIARY_ID + " INTEGER, " + REVIEWED + " boolean)");

        db.execSQL("create table if not exists " + HASHTAG_DIARIES_TABLE_NAME + "( id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DIARY_ID + " INTEGER, " + HASHTAG_ID + " INTEGER)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
