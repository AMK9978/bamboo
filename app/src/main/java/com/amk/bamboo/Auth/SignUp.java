package com.amk.bamboo.Auth;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.Models.User;
import com.amk.bamboo.ProfileActivity;
import com.amk.bamboo.R;
import com.amk.bamboo.Util;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import cn.pedant.SweetAlert.SweetAlertDialog;
import ir.hamsaa.persiandatepicker.Listener;
import ir.hamsaa.persiandatepicker.PersianDatePickerDialog;
import ir.hamsaa.persiandatepicker.util.PersianCalendar;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity {

    TextView date;
    CardView date_back;
    ImageView profileImage;
    EditText name;
    EditText password;
    RadioButton daughterButton, boyButton, unknownButton;
    TextView gotoLogin;
    Button signUp;
    private int galleryReqCode = 100;
    private int permissionCode = 101;
    private APIInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        date = findViewById(R.id.date_text);
        date_back = findViewById(R.id.date_back);
        profileImage = findViewById(R.id.profile_image);
        name = findViewById(R.id.name);
        password = findViewById(R.id.password);
        daughterButton = findViewById(R.id.radioFemale);
        boyButton = findViewById(R.id.radioMale);
        apiInterface = APIClient.getRetrofit().create(APIInterface.class);
        unknownButton = findViewById(R.id.radioUnknown);
        gotoLogin = findViewById(R.id.gotoLogin);
        signUp = findViewById(R.id.signUpBtn);

        date_back.setOnClickListener(view -> showTimePicker());
        profileImage.setOnClickListener(view -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED) {
                    //permission denied
                    String permissions = Manifest.permission.READ_EXTERNAL_STORAGE;
                    //show popup to request runtime permission
                    requestPermissions(new String[]{permissions}, permissionCode);
                } else {
                    //permission already granted
                    pickImageFromGallery();
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
        });

        gotoLogin.setOnClickListener(view -> {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        });

        signUp.setOnClickListener(view -> {
            if (validation()) {
                String password_ = password.getText().toString();
                String name_ = name.getText().toString();
                String gender = getRadio();
                String date_ = date.getText().toString();

                apiInterface.signUp(name_, Util.sharedPreferences.getString("mobile", "nothing"),
                        password_, date_, gender).enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                        if (response.isSuccessful()) {
                            saveIntoStorage(profileImage.getDrawable());
                            Util.sharedPreferences.edit().putString("password", password_).putString("birthday", date_)
                                    .putString("gender", gender).putString("name", name_)
                                    .putString("jwt", response.body().getJwt()).commit();
                            Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                            startActivity(intent);
                            finish();
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {

                    }
                });
            } else {
                launchDialog(-1);
            }
        });

    }

    private String getRadio() {
        if (daughterButton.isChecked()) {
            return "girl";
        } else if (boyButton.isChecked()) {
            return "boy";
        } else {
            return "unknown";
        }
    }

    private void launchDialog(int i) {
        SweetAlertDialog dialog = Util.getSweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);

        if (i == -1) {
            dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.red_fail));
            dialog.setTitleText(R.string.error);
        } else {
            dialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.purple_progress));
            dialog.setTitleText(R.string.wait);
        }
        dialog.showContentText(false);
        dialog.show();
        dialog.setConfirmClickListener(view -> dialog.dismiss());
    }

    private boolean validation() {
        return name.getText() != null && password.getText() != null;
    }


    private void showTimePicker() {
        PersianCalendar initDate = new PersianCalendar();
        initDate.setPersianDate(1398, 1, 1);
        PersianDatePickerDialog picker = new PersianDatePickerDialog(this)
                .setPositiveButtonString("باشه")
                .setNegativeButton("بیخیال")
                .setTodayButton("امروز")
                .setTodayButtonVisible(false)
                .setMinYear(1393)
                .setMaxYear(PersianDatePickerDialog.THIS_YEAR)
                .setInitDate(initDate)
                .setActionTextColor(Color.GRAY)
                .setTypeFace(Util.iranSans)
                .setTitleType(PersianDatePickerDialog.WEEKDAY_DAY_MONTH_YEAR)
                .setShowInBottomSheet(true)
                .setListener(new Listener() {
                    @Override
                    public void onDateSelected(PersianCalendar persianCalendar) {
                        date.setText(persianCalendar.getPersianYear() + "/" + persianCalendar.getPersianMonth() + "/" + persianCalendar.getPersianDay());
                    }

                    @Override
                    public void onDismissed() {

                    }
                });

        picker.show();
    }

    private void saveIntoStorage(Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        File file = new File(Environment.getExternalStorageDirectory(), "ChandMahame");
        if (!file.exists())
            file.mkdir();
        String path = "profileImage.jpg";
        File myPathFile = new File(file.getAbsolutePath() + "/" + path);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(myPathFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            if (data != null) {
                Log.i("TAG", String.valueOf(data.getData()));
                Picasso.get().load(data.getData()).fit().placeholder
                        (R.drawable.ic_person_silverlight_24dp).into(profileImage);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionCode == requestCode) {
            if (grantResults.length != 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                pickImageFromGallery();
            } else {
                //permission from popup denied
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, galleryReqCode);
    }
}
