package com.amk.bamboo.Auth;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.LocaleHelper;
import com.amk.bamboo.Models.User;
import com.amk.bamboo.ProfileActivity;
import com.amk.bamboo.R;
import com.amk.bamboo.Temp;
import com.amk.bamboo.Util;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private Button submitBtn;
    private EditText phoneTxt;
    private String phone = "";
    private TextView persianLang;
    private TextView loginInstruction;
    private TextView timerTxt;
    private TextView changePhoneNumber;
    private volatile boolean interrupted = false;
    private Thread threadCounterSMSVerify;
    private TextView milestone;
    private APIInterface apiInterface;
    private LinearLayout timerBack;
    private boolean phone_send = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        submitBtn = findViewById(R.id.submitBtn);
        phoneTxt = findViewById(R.id.phone_txt);
        timerTxt = findViewById(R.id.timer);
        timerBack = findViewById(R.id.timer_back);
        loginInstruction = findViewById(R.id.login_instruction);
        persianLang = findViewById(R.id.persianLang);
        milestone = findViewById(R.id.milestone_);
        changePhoneNumber = findViewById(R.id.changePhoneNumber);

        milestone.setOnClickListener(view -> {
            Intent intent = new Intent(this, SignUp.class);
            startActivity(intent);
        });
        apiInterface = APIClient.getRetrofit().create(APIInterface.class);
        SweetAlertDialog dialog = Util.getSweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        submitBtn.setOnClickListener(view -> {
            phone = phoneTxt.getText().toString().trim();
            if (phone_send) {
                if (checkPhoneNumber(phone)) {
                    sendPhoneNumber();
                } else {
                    dialog.setTitle(getResources().getString(R.string.edit_your_phone_number));
                    dialog.setContentText(getResources().getString(R.string.edit_your_phone_number));
                    dialog.show();
                    dialog.setConfirmClickListener(sweetAlertDialog -> dialog.dismiss());
                }
            } else {
                sendVerificationCode();
            }
        });

        persianLang.setOnClickListener(view -> {
            Locale current = getResources().getConfiguration().locale;
            Log.i("TAG", current.getLanguage());
            if (current.getLanguage().equals("en")) {
                setLocale("fa");
            } else {
                setLocale("en");
            }
        });

        changePhoneNumber.setOnClickListener(view -> {
            changePhoneNumber.setVisibility(View.GONE);
            persianLang.setVisibility(View.VISIBLE);
            timerBack.setVisibility(View.GONE);
            submitBtn.setText(R.string.submitBtn);
            if (threadCounterSMSVerify.isAlive())
                threadCounterSMSVerify.destroy();
        });
    }

    private boolean checkPhoneNumber(String text) {
        return true;
    }

    public void setLocale(String lang) {
        Locale current = getResources().getConfiguration().locale;
        Log.i("TAG", current.toString() + "," + lang);
        LocaleHelper.setLocale(this, lang);
        recreate();
    }

    private void startCounterTimerText() {
        threadCounterSMSVerify = new Thread(() -> {
            int timer = 59;
            while (!interrupted) {
                timerTxt.setVisibility(View.VISIBLE);
                try {
                    while (timer != 0) {
                        timer--;
                        final int finalTimer = timer;
                        runOnUiThread(() -> timerTxt.setText("00:" + finalTimer));
                        Thread.sleep(1000);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        threadCounterSMSVerify.start();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    private void sendPhoneNumber() {
        if (Util.isNetworkConnected(LoginActivity.this)) {
            submitBtn.setEnabled(false);
            phoneTxt.setEnabled(false);
            SweetAlertDialog dialog = Util.getSweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            dialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.green_success));
            dialog.setTitleText(R.string.wait);
            dialog.showContentText(false);
            dialog.show();
            // note: when user click back button , maybe this task run, and app crash
            apiInterface.sendPhoneNumber(phone).enqueue(new Callback<Temp>() {
                @Override
                public void onResponse(@NotNull Call<Temp> call, @NotNull Response<Temp> response) {
                    dialog.dismiss();
                    submitBtn.setEnabled(true);
                    phoneTxt.setEnabled(true);
                    if (response.isSuccessful()) {
                        startCounterTimerText();
                        changePhoneNumber.setVisibility(View.VISIBLE);
                        persianLang.setVisibility(View.GONE);
                        timerBack.setVisibility(View.VISIBLE);
                        phoneTxt.setText("");
                        submitBtn.setText(R.string.verify_code);
                        Util.sharedPreferences.edit().putString("mobile", phone).commit();
                        phone_send = false;
                        loginInstruction.setText(getResources().getString(R.string.input_code));
                    } else {
                        Log.i("TAG", "In onResponse of sendPhone, wasn't successful"
                                + response.message() + " " + response.body() + " " + response.code());
                    }
                }

                @Override
                public void onFailure(@NotNull Call<Temp> call, @NotNull Throwable t) {
                    dialog.dismiss();
                    Log.i("TAG", "In Error of sendPhone " + t.getMessage());
                    submitBtn.setEnabled(true);
                    phoneTxt.setEnabled(true);
                }
            });
        } else {
            SweetAlertDialog dialog = Util.getSweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.red_fail));
            dialog.setTitleText(R.string.error);
            dialog.setContentText(getResources().getString(R.string.connection_error));
            dialog.show();
            dialog.setConfirmClickListener(view -> dialog.dismiss());
        }
    }


    private void sendVerificationCode() {
        if (Util.isNetworkConnected(LoginActivity.this)) {
            SweetAlertDialog dialog = Util.getSweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            dialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
            dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.green_success));
            dialog.setTitleText(R.string.wait);
            dialog.showContentText(false);
            dialog.show();
            String mobile = Util.sharedPreferences.getString("mobile", "nothing");
            String code = phoneTxt.getText().toString();
            Log.i("TAG", mobile);
            Log.i("TAG", code);
            // note: when user click back button , maybe this task run, and app crash
            apiInterface.sendVerificationCode(mobile,
                    code).enqueue(new Callback<User>() {
                @Override
                public void onResponse(@NotNull Call<User> call, @NotNull Response<User> response) {
                    dialog.dismiss();
                    if (response.isSuccessful()) {
                        Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
//                        intent.putExtra("user", response.body());
                        Util.sharedPreferences.edit().putString("jwt", response.body().getJwt())
                                .putString("mobile", response.body().getMobile())
                                .putString("name", response.body().getName()).commit();
                        startActivity(intent);
                        finish();
                    } else if (response.code() == 401) {
                        Intent intent = new Intent(getApplicationContext(), SignUp.class);
//                       intent.putExtra("user", response);
                        startActivity(intent);
                        finish();
                    } else {
                        Log.i("TAG", "Error in sendVerification " +
                                response.code() + " " + response.message() + " " + response.body());
                    }
                }

                @Override
                public void onFailure(@NotNull Call<User> call, @NotNull Throwable t) {
                    dialog.dismiss();
                    Log.i("TAG", "In sendVerification, onError" + t.getMessage());
                }
            });

        } else {
            SweetAlertDialog dialog = Util.getSweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE);
            dialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            dialog.getProgressHelper().setBarColor(getResources().getColor(R.color.red_fail));
            dialog.setTitleText(R.string.error);
            dialog.setContentText(getResources().getString(R.string.connection_error));
            dialog.show();
            dialog.setConfirmClickListener(view -> dialog.dismiss());
        }
    }


}
