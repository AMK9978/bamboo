package com.amk.bamboo;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class LocationChoosing extends AppCompatActivity {


    private ImageView backBtn;
    private TextView totalPrice, changeLocation, choose_location, postal_code, location_address,
            name;
    private RadioButton vangaurd, custom;
    private Button goAhead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_choosing);
        backBtn = findViewById(R.id.backBtn);
        totalPrice = findViewById(R.id.total_price);
        changeLocation = findViewById(R.id.changeLocation);
        choose_location = findViewById(R.id.choose_location);
        postal_code = findViewById(R.id.postal_code);
        location_address = findViewById(R.id.location_address);
        name = findViewById(R.id.name);
        custom = findViewById(R.id.radioCustom);
        vangaurd = findViewById(R.id.radioVanguard);
        goAhead = findViewById(R.id.goAhead);
        goAhead.setOnClickListener(v -> {
            //TODO: Go to webView
//            Intent intent = new Intent(this, )
        });

        backBtn.setOnClickListener(v -> {
            onBackPressed();
            finish();
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
