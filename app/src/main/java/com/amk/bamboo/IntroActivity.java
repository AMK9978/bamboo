package com.amk.bamboo;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.amk.bamboo.Auth.LoginActivity;
import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

public class IntroActivity extends AppIntro {


    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sharedPreferences = Util.sharedPreferences;
        super.onCreate(savedInstanceState);
        if (Util.sharedPreferences.getBoolean("isShowedIntroApp", false)) {
            if (sharedPreferences.getString("jwt", "nothing").equals("nothing")) {
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            }
            finish();
        }
        SliderPage sliderPage = new SliderPage();
        sliderPage.setTitle("چندماهمه");
        sliderPage.setDescription("خاطرات تان را به راحتی ثبت کنید!");
        sliderPage.setBgColor(getResources().getColor(R.color.black));
        sliderPage.setImageDrawable(R.drawable.logo);
        sliderPage.setBgColor(getResources().getColor(R.color.white));
        sliderPage.setTitleTypeface("fonts/iran_sans_normal.ttf");
        sliderPage.setDescTypeface("fonts/iran_sans_normal.ttf");
        addSlide(AppIntroFragment.newInstance(sliderPage));
        SliderPage sliderPage2 = new SliderPage();
        sliderPage2.setTitle("دفترهای خاطرات");
        sliderPage2.setImageDrawable(R.drawable.baner_albumeha);
        sliderPage2.setBgColor(getResources().getColor(R.color.white));
        sliderPage2.setTitleTypeface("fonts/iran_sans_normal.ttf");
        sliderPage2.setTitleColor(getResources().getColor(R.color.colorPrimaryDark));
        sliderPage2.setDescColor(getResources().getColor(R.color.analogous));
        sliderPage2.setDescTypeface("fonts/iran_sans_normal.ttf");
        addSlide(AppIntroFragment.newInstance(sliderPage2));
        SliderPage sliderPage3 = new SliderPage();
        sliderPage3.setTitle("تخته سیاه");
        sliderPage3.setTitleTypeface("fonts/iran_sans_normal.ttf");
        sliderPage3.setDescTypeface("fonts/iran_sans_normal.ttf");
        sliderPage3.setImageDrawable(R.drawable.baner_takhteh);
        sliderPage3.setBgColor(getResources().getColor(R.color.white));
        addSlide(AppIntroFragment.newInstance(sliderPage3));
        setSlideOverAnimation();
        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(getResources().getColor(R.color.colorPrimaryDark));

        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

        // Turn vibration on and set intensity.
        // NOTE: you will probably need to ask VIBRATE permission in Manifest.
        setVibrate(false);
        setVibrateIntensity(30);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
        sharedPreferences.edit().putBoolean("isShowedIntroApp", true).commit();
        if (Util.sharedPreferences.getBoolean("isShowedIntroApp", false)) {
            if (sharedPreferences.getString("jwt", "nothing").equals("nothing")) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            }
            finish();
        }
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        sharedPreferences.edit().putBoolean("isShowedIntroApp", true).commit();

        if (Util.sharedPreferences.getBoolean("isShowedIntroApp", false)) {
            if (sharedPreferences.getString("jwt", "nothing").equals("nothing")) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
            }
            finish();
        }
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
