package com.amk.bamboo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ShareCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.amk.bamboo.API.APIClient;
import com.amk.bamboo.API.APIInterface;
import com.amk.bamboo.API.InitHomeRequest;
import com.amk.bamboo.DashboardFragments.BlogFragment;
import com.amk.bamboo.DashboardFragments.HomeFragment;
import com.amk.bamboo.DashboardFragments.PrintFragment;
import com.amk.bamboo.DashboardFragments.ProductsFragment;
import com.amk.bamboo.DashboardFragments.RegisterDiaryFragment;
import com.amk.bamboo.Models.User;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import org.jetbrains.annotations.NotNull;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    private DrawerLayout drawer_layout;
    private NavigationView navigationView;
    private FirebaseAnalytics firebaseAnalytics;
    private ImageView menu_btn;
    private ActionBarDrawerToggle drawerToggle;
    private TextView self_name;
    private int galleryReqCode = 100;
    private int permissionCode = 101;
    private CircleImageView profileImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        drawer_layout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        profileImage = header.findViewById(R.id.profile_image);
        self_name = header.findViewById(R.id.self_name);
        bottomNavigationView = findViewById(R.id.bottom);
        loadProfileImage();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawerToggle = new ActionBarDrawerToggle(this, drawer_layout, R.string.title, R.string.title);
        drawer_layout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(item -> {
            Fragment fragment;
            if (item.getItemId() == R.id.nav_home) {
                fragment = new HomeFragment();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            } else if (item.getItemId() == R.id.nav_basket) {
                Intent intent = new Intent(this, BasketActivity.class);
                startActivity(intent);
            } else if (item.getItemId() == R.id.nav_share) {
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "https://morris_fa.ir/morris.apk");
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, Build.VERSION.RELEASE);
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle);
                share();
            } else if (item.getItemId() == R.id.nav_contact) {
                Intent intent = new Intent(this, ContactActivity.class);
                startActivity(intent);
            } else if (item.getItemId() == R.id.nav_gallery) {
                Intent intent = new Intent(this, GalleryActivity.class);
                startActivity(intent);
            } else if (item.getItemId() == R.id.nav_print) {
                fragment = PrintFragment.newInstance();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            }
            drawer_layout.closeDrawer(GravityCompat.START);
            return false;
        });
        navigationView.setScrollBarFadeDuration(5000);
        bottomNavigationView.setBackgroundColor(getResources().getColor(R.color.white));
        loadFragment(new HomeFragment());
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        bottomNavigationView.setSelectedItemId(R.id.homepage);
        menu_btn = findViewById(R.id.menu_btn);
        menu_btn.setOnClickListener(v -> {
            if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
                drawer_layout.closeDrawer(GravityCompat.START);
            } else {
                drawer_layout.openDrawer(GravityCompat.START);
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            Fragment fragment;
            int id = item.getItemId();
            if (id == R.id.homepage) {
                fragment = new HomeFragment();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            } else if (id == R.id.blog) {
                fragment = new BlogFragment();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            } else if (id == R.id.print_image) {
                fragment = new PrintFragment();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            } else if (id == R.id.products) {
                fragment = new ProductsFragment();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            } else if (id == R.id.register_diary) {
                fragment = new RegisterDiaryFragment();
                getSupportFragmentManager().popBackStack();
                loadFragment(fragment);
            }
            return true;
        });

        APIInterface apiInterface = APIClient.getRetrofit().create(APIInterface.class);

        apiInterface.getInitHome().enqueue(new Callback<InitHomeRequest>() {
            @Override
            public void onResponse(@NotNull Call<InitHomeRequest> call, @NotNull Response<InitHomeRequest> response) {
                if (response.isSuccessful() && response.body() != null) {
                    set(response.body());
                } else {
                    Log.i("TAG", "Error in onResponse of Products " + response.code());
                }
            }

            @Override
            public void onFailure(@NotNull Call<InitHomeRequest> call, @NotNull Throwable t) {
                Log.i("TAGd", t.getMessage());
            }
        });
        profileImage.setOnClickListener(v -> {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_DENIED) {
                    //permission denied
                    String permissions = Manifest.permission.READ_EXTERNAL_STORAGE;
                    //show popup to request runtime permission
                    requestPermissions(new String[]{permissions}, permissionCode);
                } else {
                    //permission already granted
                    pickImageFromGallery();
                }
            } else {
                //system OS is < Marshmallow
                pickImageFromGallery();
            }
        });
    }


    private void loadProfileImage() {
        File mainFile = new File(Environment.getExternalStorageDirectory(), "Chandmahame/profileImage.png");
        Picasso.get().load(mainFile).fit().placeholder(R.drawable.ic_person_outline_blue_48dp).into(profileImage);
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, galleryReqCode);
    }

    private void set(InitHomeRequest body) {
        User user = body.getUser();
        self_name.setText(user.getName());
        Picasso.get().load(Uri.parse(user.getImagePath())).placeholder(R.drawable.ic_person_silverlight_24dp).into(profileImage);
    }


    private void share() {
        ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setChooserTitle("اشتراک گذاری چندماهمه")
                .setText("https://morris_fa.ir/morris.apk")
                .startChooser();
    }


    public void loadFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (getIntent() != null) {
//            String gotoRegister = getIntent().getStringExtra("gotoRegister");
//            if (gotoRegister != null) {
//                bottomNavigationView.setSelectedItemId(R.id.register_diary);
//            }
//            String gotoPrint = getIntent().getStringExtra("gotoPrint");
//            if (gotoPrint != null) {
//                bottomNavigationView.setSelectedItemId(R.id.print_image);
//            }
//            String gotoProducts = getIntent().getStringExtra("gotoProducts");
//            if (gotoProducts != null) {
//                bottomNavigationView.setSelectedItemId(R.id.products);
//            }
//            String gotoBlog = getIntent().getStringExtra("gotoBlog");
//            if (gotoBlog != null) {
//                bottomNavigationView.setSelectedItemId(R.id.blog);
//            }
//        }
    }


    private void performCrop(Uri picUri) {
        Log.i("TAG", String.valueOf(picUri));
        File file = new File(Environment.getExternalStorageDirectory(), "ChandMahame");
        String ss = "profileImage.png";
        File myPathFile = new File(file.getAbsolutePath() + "/" + ss);
        UCrop.Options options = new UCrop.Options();
        options.setToolbarTitle("برش تصویر");
        options.setToolbarColor(getResources().getColor(R.color.colorPrimary));
        options.setLogoColor(getResources().getColor(R.color.colorPrimary));
        options.setStatusBarColor(getResources().getColor(R.color.white));
        options.setActiveWidgetColor(getResources().getColor(R.color.colorPrimary));
        options.setCompressionFormat(Bitmap.CompressFormat.PNG);
        options.setToolbarWidgetColor(getResources().getColor(R.color.white));
        options.setActiveWidgetColor(getResources().getColor(R.color.colorPrimary));
        UCrop.of(picUri, Uri.fromFile(myPathFile)).withOptions(options).withAspectRatio(1, 1)
                .start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
                final Uri resultUri = UCrop.getOutput(data);
                Picasso.get().load(resultUri).placeholder(R.drawable.ic_person_outline_blue_48dp).into(profileImage);
            } else if (resultCode == UCrop.RESULT_ERROR) {
                final Throwable cropError = UCrop.getError(data);
                Log.i("TAGgg", cropError.getMessage());
            }
            if (resultCode == RESULT_OK && requestCode == galleryReqCode) {
                if (data.getData() != null)
                    performCrop(data.getData());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionCode == requestCode) {
            if (grantResults.length != 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                //permission from popup granted
                pickImageFromGallery();
            } else {
                //permission from popup denied
                Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

}
