package com.amk.bamboo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class BasketActivity extends AppCompatActivity {

    private ImageView backBtn;
    private TextView totalPrice, productCount;
    private Button goAhead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket);
        backBtn = findViewById(R.id.backBtn);
        totalPrice = findViewById(R.id.total_price);
        productCount = findViewById(R.id.productCount);
        goAhead = findViewById(R.id.goAhead);
        goAhead.setOnClickListener(v -> {
            Intent intent = new Intent(this, LocationChoosing.class);
            startActivity(intent);
        });
        backBtn.setOnClickListener(v -> {
            onBackPressed();
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
