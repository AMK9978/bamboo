package com.amk.bamboo;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

public class EventFragment extends Fragment {

    ImageView image;
    private Uri uri;

    public EventFragment(Uri uri) {
        this.uri = uri;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.banner_layout, container, false);
        image = v.findViewById(R.id.banner_image);
        Picasso.get().load(uri).fit().placeholder(R.drawable.albumma).into(image);
        return v;
    }
}